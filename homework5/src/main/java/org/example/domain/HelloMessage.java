package org.example.domain;

import lombok.Data;

@Data
public class HelloMessage {
    private String name;
}

