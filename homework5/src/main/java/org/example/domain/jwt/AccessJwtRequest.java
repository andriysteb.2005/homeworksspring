package org.example.domain.jwt;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccessJwtRequest {
    private String accessToken;
}

