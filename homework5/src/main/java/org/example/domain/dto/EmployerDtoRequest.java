package org.example.domain.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmployerDtoRequest {
    @NotNull
    @Size(min = 3, message = "Name must have at least 3 characters")
    private String name;
    @NotNull
    @Size(min = 3, message = "Address must have at least 3 characters")
    private String address;

}
