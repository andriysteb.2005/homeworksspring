package org.example.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDtoResponse {
    private Long id;
    private String name;
    private String email;
    private Integer age;
    private String phoneNum;

}