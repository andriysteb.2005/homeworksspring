package org.example.domain.dto;

import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.example.domain.Currency;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccountDtoRequest {
    @NotBlank(message = "Number is required")
    private String number;

    @NotNull(message = "Currency is required")
    private Currency currency;

    @DecimalMin(value = "0", inclusive = false, message = "Balance can be negative")
    private Double balance;

    @NotNull(message = "Customer ID is required")
    private Long customerId;
}
