package org.example.domain.dto;

import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDtoRequest {
    @NotNull
    @Size(min = 2, message = "name should have at least 2 characters")
    private String name;

    @NotNull
    @Email(message = "email is incorrect")
    private String email;

    @NotNull
    @Min(value = 18, message = "Age should be 18+")
    private Integer age;

    @Pattern(regexp = "(\\+380|0)[0-9]{9}")
    @NotBlank
    private String phone;
    private String password;


}
