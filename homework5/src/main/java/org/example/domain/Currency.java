package org.example.domain;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.lang3.EnumUtils;

public enum Currency {
    USD("USD"),
    EUR("EUR"),
    UAH("UAH"),
    CHF("CHF"),
    GBP("GBP");

    private String val;

    Currency(String val) {
        this.val = val;
    }

    @JsonValue
    public String getValue() {
        return val;
    }
    @JsonCreator
    public static Currency forValue(String name)
    {
        return EnumUtils.getEnumMap(Currency.class).get(name);
    }

}
