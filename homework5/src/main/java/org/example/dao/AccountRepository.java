package org.example.dao;

import org.example.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AccountRepository extends JpaRepository<Account, Long> {
    @Query("select e from Account e where e.number = :number")
    Account findByNumber(String number);
}


