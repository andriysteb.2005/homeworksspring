package org.example.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.domain.Employer;
import org.example.domain.dto.EmployerDtoRequest;
import org.example.service.EmployerService;
import org.example.service.dto.EmployerDtoRequestMapper;
import org.example.service.dto.EmployerDtoResponseMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = {"http://localhost:3000"})
@RequestMapping("/employers")
@RequiredArgsConstructor
@Slf4j
public class EmployerController {

    private final EmployerService employerService;
    private final EmployerDtoRequestMapper employerDtoRequestMapper;
    private final EmployerDtoResponseMapper employerDtoResponseMapper;

    @GetMapping("/")
    public ResponseEntity<?> getAllEmployers(){
        log.info("Find all method");
        return ResponseEntity.ok(employerService.getAllEmployers().stream()
                .map(employerDtoResponseMapper::convertToDto)
                .collect(Collectors.toList()));
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getEmployerFullInfo(@PathVariable Long id){
        Employer employer = employerService.getEmployerFullInfo(id);
        log.info("Find by id method");
        return employer != null
                ? ResponseEntity.ok().body(employerDtoResponseMapper.convertToDto(employer))
                : ResponseEntity.badRequest().body("Employer with id " + id + " not found");
    }
    @PostMapping("/")
    public ResponseEntity<?> postNewEmployer(@RequestBody EmployerDtoRequest employerDtoRequest){
        Employer employer = employerDtoRequestMapper.convertToEntity(employerDtoRequest);
        Employer newEmployer = employerService.createEmployer(employer);
        log.info("Employer created");
        return newEmployer != null
                ? ResponseEntity.ok().body(employerDtoResponseMapper.convertToDto(newEmployer))
                : ResponseEntity.badRequest().body("Saving of the new employer has fail");
    }
    @PutMapping("/")
    public ResponseEntity<?> updateEmployer(@RequestBody EmployerDtoRequest employerDtoRequest){
        Employer employer = employerDtoRequestMapper.convertToEntity(employerDtoRequest);
        Employer newEmployer = employerService.updateEmployer(employer);
        log.info("Employer updated");
        return newEmployer != null
                ? ResponseEntity.ok().body(employerDtoResponseMapper.convertToDto(newEmployer))
                : ResponseEntity.badRequest().body("Updating of the employer has fail");
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteEmployer(@PathVariable Long id){
        log.info("Employer deleted");
        return employerService.deleteEmployerById(id)
                ? ResponseEntity.ok("Success")
                : ResponseEntity.badRequest().body("Employer not found");
    }
    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleExceptions(Exception e){
        log.warn(e.getMessage());
        return ResponseEntity.status(400).body(e.getMessage());
    }
}

