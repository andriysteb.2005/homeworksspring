package org.example.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.domain.Customer;
import org.example.domain.Currency;
import org.example.domain.dto.CustomerDtoRequest;
import org.example.domain.dto.CustomerDtoResponse;
import org.example.service.CustomerService;
import org.example.service.dto.CustomerDtoRequestMapper;
import org.example.service.dto.CustomerDtoResponseMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = {"http://localhost:3000"})
@RequestMapping("/customers")
@RequiredArgsConstructor
@Slf4j
public class CustomerController {

    private final CustomerService customerService;
    private final CustomerDtoResponseMapper customerDtoResponseMapper;
    private final CustomerDtoRequestMapper customerDtoRequestMapper;

    @GetMapping("/")
    public ResponseEntity<?> getAllUsers() {
        log.info("find all method");
        return ResponseEntity.ok(customerService.getAllCustomers().stream()
                .map(customerDtoResponseMapper::convertToDto)
                .collect(Collectors.toList()));
    }
    @GetMapping("/{page}/{size}")
    public ResponseEntity<?> getAllUsers(@PathVariable Integer page, @PathVariable Integer size) {
        log.info("find all pageable method");
        return ResponseEntity.ok(customerService.getAllCustomers(page, size).stream()
                .map(customerDtoResponseMapper::convertToDto)
                .collect(Collectors.toList()));
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getUserFullInfo(@PathVariable Long id){
        log.info("Find by ID method");
        return ResponseEntity.ok(customerDtoResponseMapper.convertToDto(customerService.getCustomerFullInfo(id)));
    }
    @PostMapping("/")
    public CustomerDtoResponse postNewUser(@RequestBody CustomerDtoRequest customerDtoRequest){
        Customer customer = customerDtoRequestMapper.convertToEntity(customerDtoRequest);
        log.info("User created");
        return customerDtoResponseMapper.convertToDto(customerService.createCustomer(customer));
    }
    @PutMapping("/")
    public CustomerDtoResponse updateUser(@RequestBody CustomerDtoRequest customerDtoRequest){
        log.info("User updated");
        Customer customer = customerDtoRequestMapper.convertToEntity(customerDtoRequest);
        return customerDtoResponseMapper.convertToDto(customerService.updateCustomer(customer));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable Long id){
        log.info("User deleted");
        return customerService.deleteCustomerById(id) ? ResponseEntity.ok("Success") : ResponseEntity.badRequest().body("Customer not found");
    }
    @PostMapping("/{id}/account")
    public ResponseEntity<?> openAccount(@PathVariable Long id, @RequestBody String currency) throws JsonProcessingException {

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode newAccountCurrency = objectMapper.readTree(currency);
        String curren = newAccountCurrency.get("currency").asText();
        Currency curr = Currency.forValue(curren);

        Customer customer = customerService.getCustomerFullInfo(id);
        if(customer == null) {
            ResponseEntity.badRequest().body("Customer not found");
        }
        return ResponseEntity.ok(customerDtoResponseMapper.convertToDto(customerService.openAccount(customer, curr)));
    }
    @DeleteMapping("/{id}/account")
    public ResponseEntity<?> deleteAccount(@PathVariable Long id, @RequestBody String accountNumber ) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode nameNode = mapper.readTree(accountNumber);
        Customer customer = customerService.getCustomerFullInfo(id);
        if(customer == null) {
            ResponseEntity.badRequest().body("Customer not found");
        }
        return ResponseEntity.ok(customerDtoResponseMapper.convertToDto(customerService.deleteAccount(customer, nameNode.get("accountNumber").asText())));
    }
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> handleValidationErrors(MethodArgumentNotValidException e){
        List<String> errors = e.getBindingResult().getFieldErrors().stream().map(FieldError::getDefaultMessage).toList();
        log.warn(getErrorsMap(errors).toString());
        return ResponseEntity.status(400).body(getErrorsMap(errors));
    }


    private Map<String, List<String>> getErrorsMap(List<String> errors) {
        Map<String, List<String>> errorResponse = new HashMap<>();
        errorResponse.put("errors", errors);
        return errorResponse;
    }

}


