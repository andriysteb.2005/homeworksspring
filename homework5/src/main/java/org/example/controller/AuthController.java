package org.example.controller;


import jakarta.security.auth.message.AuthException;
import lombok.RequiredArgsConstructor;
import org.example.domain.jwt.AccessJwtRequest;
import org.example.domain.jwt.JwtRequest;
import org.example.domain.jwt.JwtResponse;
import org.example.domain.jwt.RefreshJwtRequest;
import org.example.service.AuthService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<JwtResponse> login(@RequestBody JwtRequest authRequest) throws AuthException {
        final JwtResponse token = authService.login(authRequest);
        return ResponseEntity.ok(token);
    }

    @PostMapping("/token")
    public ResponseEntity<JwtResponse> getNewAccessToken(@RequestBody RefreshJwtRequest request) throws AuthException {
        final JwtResponse token = authService.getAccessToken(request.getRefreshToken());
        return ResponseEntity.ok(token);
    }

    @PostMapping("/refresh")
    public ResponseEntity<JwtResponse> getNewRefreshToken(@RequestBody RefreshJwtRequest request) throws AuthException {
        final JwtResponse token = authService.refresh(request.getRefreshToken());
        return ResponseEntity.ok(token);
    }

    @PostMapping("/invalidate")
    public ResponseEntity<?> invalidateAccessToken(@RequestBody AccessJwtRequest access){
        authService.blockAccess(access.getAccessToken());
        return ResponseEntity.ok("Token invalidated");
    }




}
