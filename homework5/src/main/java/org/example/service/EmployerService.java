package org.example.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.example.dao.EmployerRepository;
import org.example.domain.Employer;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@RequiredArgsConstructor
@Transactional
public class EmployerService {

    private final EmployerRepository employerRepository;

    public Employer getEmployerFullInfo(Long id){
        return employerRepository.getOne(id);
    }

    public List<Employer> getAllEmployers(){
        return employerRepository.findAll();
    }

    public Employer createEmployer(Employer newEmployer){
        return employerRepository.save(newEmployer);
    }
    public Employer createEmployer(String name, String address){
        Employer newEmployer = new Employer(name, address);
        return employerRepository.save(newEmployer);
    }

    public Employer updateEmployer(Employer employer){
        if(employer.getId() == null){
            return null;
        }
        return employerRepository.save(employer);
    }

    public Employer updateEmployer(Long id, String name, String address){
        if(id == null){
            return null;
        }
        Employer employer = new Employer(id, name, address);
        return employerRepository.save(employer);
    }

    public void deleteEmployer(Employer employer){
        employerRepository.delete(employer);
    }
    public boolean deleteEmployerById(Long id){
        Employer currentEmployer = this.getEmployerFullInfo(id);
        this.deleteEmployer(currentEmployer);
        return true;
    }
}

