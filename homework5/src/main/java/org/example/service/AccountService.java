package org.example.service;


import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.example.dao.AccountRepository;
import org.example.domain.Account;
import org.example.domain.Greeting;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@RequiredArgsConstructor
@Transactional
public class AccountService {
    private final AccountRepository accountRepository;
    private SimpMessagingTemplate simpMessagingTemplate;

    public boolean depositAccount(String number, Double amount) throws IOException, InterruptedException {
        Account account = accountRepository.findByNumber(number);

        if(account != null){
            simpMessagingTemplate.convertAndSend("/topic/hello.user",
                    new Greeting("Account number " + number + " is diposited with ammount " + amount ));
            return this.depositAccount(account, amount);
        }
        return false;
    }
    public boolean depositAccount(Account account, Double amount){
        account.setBalance(account.getBalance() + amount);
        accountRepository.save(account);
        return true;
    }
    public boolean withdrawalMoney(String number, Double amount){
        System.out.println(number);
        System.out.println(amount);
        Account account = accountRepository.findByNumber(number);
        if(account.getBalance() >= amount){
            return this.withdrawalMoney(account, amount);
        }
        return false;
    }
    public boolean withdrawalMoney(Account account, Double amount){
        if(account.getBalance() >= amount){
            account.setBalance(account.getBalance() - amount);
            accountRepository.save(account);
            return true;
        }
        return false;
    }

    public boolean transferMoney(String numberSender, String numberReceiver, Double amount){
        Account accountSender = accountRepository.findByNumber(numberSender);
        Account accountReceiver = accountRepository.findByNumber(numberReceiver);
        if(!accountSender.getCurrency().equals(accountReceiver.getCurrency())){
            return false;
        }
        if(!this.withdrawalMoney(accountSender, amount)){
            return false;
        };
        if(!this.depositAccount(accountReceiver, amount)){
            this.depositAccount(accountSender, amount);
            return false;
        };
        return true;
    }
}


