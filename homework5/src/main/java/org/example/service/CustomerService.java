package org.example.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.example.dao.AccountRepository;
import org.example.dao.CustomerRepository;
import org.example.domain.Account;
import org.example.domain.Currency;
import org.example.domain.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final AccountRepository accountRepository;


    public Customer getCustomerFullInfo(Long id){
        return customerRepository.findById(id).get();
    }

    public List<Customer> getAllCustomers(Integer  page, Integer size){
        Sort sort = Sort.by(new Sort.Order(Sort.Direction.ASC, "id"));
        Pageable pageable = PageRequest.of(page, size, sort);
        Page<Customer> customerPage = customerRepository.findAll(pageable);
        return customerPage.toList();
    }
    public List<Customer> getAllCustomers(){
        return getAllCustomers(0, 100);
    }
    public Customer createCustomer(Customer newCustomer){
        return customerRepository.save(newCustomer);
    }
    public Customer createCustomer(String name, String email, Integer age){
        Customer newCustomer = new Customer(name, email, age);
        return customerRepository.save(newCustomer);
    }

    public Customer updateCustomer(Customer customer){
        if(customer.getId() == null){
            return null;
        }
        return customerRepository.save(customer);
    }

    public Customer updateCustomer(Long id, String name, String email, Integer age){
        if(id == null){
            return null;
        }
        Customer customer = new Customer(name, email, age);
        customer.setId(id);
        return customerRepository.save(customer);
    }

    public void deleteCustomer(Customer customer){
        customerRepository.delete(customer);
    }
    public boolean deleteCustomerById(Long id){
        Customer currentCustomer = this.getCustomerFullInfo(id);
        this.deleteCustomer(currentCustomer);
        return true;
    }
    public Customer openAccount(Customer customer, Currency currency){
        Account newAccount = new Account(currency, customer);
        accountRepository.save(newAccount);
        return this.getCustomerFullInfo(customer.getId());
    }
    public Customer deleteAccount(Customer customer, String number){
        Account account = accountRepository.findByNumber(number);
        if(account.getCustomer().equals(customer)){
            accountRepository.deleteById(account.getId());
        }
        return this.getCustomerFullInfo(customer.getId());
    }
}


