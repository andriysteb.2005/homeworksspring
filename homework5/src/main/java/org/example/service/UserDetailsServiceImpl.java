package org.example.service;

import lombok.RequiredArgsConstructor;
import org.example.dao.UserRepository;
import org.example.domain.Role;
import org.example.domain.User;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<User> user = userRepository.findUsersByUserName(username);
        if (user.isEmpty()) {
            throw new UsernameNotFoundException(username);
        }
        List<SimpleGrantedAuthority> authorities = user.get().getRoles().stream()
                .map(r -> new SimpleGrantedAuthority(r.getRoleName()))
                .toList();
        return new org.springframework.security.core.userdetails.User(user.get().getUserName(), user.get().getEncryptedPassword(), authorities);
    }

    public void createNew(String userName, String password) {
        Role role = new Role(null, "USER", null);
        User user = new User(null,
                userName,
                passwordEncoder.encode(password),
                true,
                Set.of(role));
        role.setUser(user);
        userRepository.save(user);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public void createNew() {
        Role role = new Role(null, "USER", null);
        User user = new User(1L,
                "new",
                passwordEncoder.encode("new"),
                true,
                Set.of(role));
        role.setUser(user);
        userRepository.save(user);
    }


}


