package org.example.service.dto;

import org.example.domain.Employer;
import org.example.domain.dto.EmployerDtoResponse;
import org.example.service.DtoMapperFacade;
import org.springframework.stereotype.Service;

@Service
public class EmployerDtoResponseMapper extends DtoMapperFacade<Employer, EmployerDtoResponse> {


    public EmployerDtoResponseMapper() {
        super(Employer.class, EmployerDtoResponse.class);
    }

    @Override
    protected void decorateDto(EmployerDtoResponse dto, Employer entity){
        dto.setAddress(entity.getAddress());
        dto.setName(entity.getName());
        dto.setId(entity.getId());
    }

}

