package org.example.service.dto;

import org.example.domain.Customer;
import org.example.domain.dto.CustomerDtoResponse;
import org.example.service.DtoMapperFacade;
import org.springframework.stereotype.Service;

@Service
public class CustomerDtoResponseMapper extends DtoMapperFacade<Customer, CustomerDtoResponse> {


    public CustomerDtoResponseMapper() {
        super(Customer.class, CustomerDtoResponse.class);
    }

    @Override
    protected void decorateDto(CustomerDtoResponse dto, Customer entity) {
        dto.setAge(entity.getAge());
        dto.setName(entity.getName());
        dto.setEmail(entity.getEmail());
        dto.setPhoneNum(entity.getPhoneNum());
        dto.setId(entity.getId());
    }
}

