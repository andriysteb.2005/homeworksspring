package org.example.service;

import lombok.extern.slf4j.Slf4j;
import org.example.domain.Greeting;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
@Slf4j
public class SchedulingService {
    private SimpMessagingTemplate simpMessagingTemplate;

    public SchedulingService(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @Scheduled(fixedDelay = 500)
    private void bgColor(){
        Random random = new Random();
        String color = String.format("#%02x%02x%02x", random.nextInt(255), random.nextInt(255), random.nextInt(255));
        simpMessagingTemplate.convertAndSend("/topic/color", new Greeting(color));
    }
}


