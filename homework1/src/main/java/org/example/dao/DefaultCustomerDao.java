package org.example.dao;

import org.example.domain.Account;
import org.example.domain.Currency;
import org.example.domain.Customer;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
@Repository
public class DefaultCustomerDao implements CustomerDao{

    private List<Customer> customers = new ArrayList<>(List.of(new Customer(1L,"bob", "d@q",20),
            new Customer(2L,"tom", "a@a",33),
            new Customer(3L,"will", "3@d",45)
    ));

    @Override
    public Customer save(Customer customer) {
        long i = 1;
        for (Customer c : customers) {
            if (i == c.getId()) {
                i++;
            }
        }
        customer.setId(i);
        customers.add(customer);
        return customer;
    }

    @Override
    public boolean delete(Customer customer) {
        return customers.remove(customer);
    }

    @Override
    public void deleteAll(List<Customer> customers) {
        customers.remove(customers);

    }

    @Override
    public void saveAll(List<Customer> customers) {
        customers.addAll(customers);

    }

    @Override
    public List<Customer> findAll() {
        return customers;
    }

    @Override
    public boolean deleteById(long id) {
        return customers.removeIf(account -> account.getId() == id);
    }

    @Override
    public Customer getOne(long id) {
        for (Customer customer : customers) {
            if (customer.getId() == id) {


                return customer;
            }
        }
        return null;
    }

    @Override
    public boolean update(long id, Customer customer){
        boolean update = false;
        for (Customer c: customers) {
            if (c.getId() == id){
                c = customer;
                c.setId(id);
                update =  true;
            }
        }
        return update;
    }

}
