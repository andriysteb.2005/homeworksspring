package org.example.dao;

import org.example.domain.Account;
import org.example.domain.Currency;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class DefaultAccountDao implements AccountDao {
    private List<Account> accounts = new ArrayList<>(List.of(new Account(1L,500000.0, Currency.USD,null),
            new Account(2L,3000000.0, Currency.USD,null),
            new Account(3L, 10000000.0, Currency.USD,null))
    );

    @Override
    public Account save(Account account) {
        long i = 1;
        for (Account a : accounts) {
            if (i == a.getId()) {
                i++;
            }
        }
        account.setId(i);
        accounts.add(account);
        return account;
    }

    @Override
    public boolean delete(Account account) {
        return accounts.remove(account);
    }

    @Override
    public void deleteAll(List<Account> accounts) {
        accounts.remove(accounts);

    }

    @Override
    public void saveAll(List<Account> accounts) {
        accounts.addAll(accounts);

    }

    @Override
    public List<Account> findAll() {
        return accounts;
    }

    @Override
    public boolean deleteById(long id) {
        return accounts.removeIf(account -> account.getId() == id);
    }

    @Override
    public Account getOne(long id) {
        for (Account account : accounts) {
            if (account.getId() == id) {
                return account;
            }
        }
        return null;
    }
    @Override
    public Account getOne(String number){
        Account account = null;
        for (Account a: accounts) {
            if (Objects.equals(a.getNumber(), number)){
                account = a;
            }
        }
        return account;
    }

    @Override
    public boolean update(long id, Account account){
        boolean update = false;
        for (Account a: accounts) {
            if (a.getId() == id){
                a = account;
                a.setId(id);
                update =  true;
            }
        }
        return update;
    }

}
