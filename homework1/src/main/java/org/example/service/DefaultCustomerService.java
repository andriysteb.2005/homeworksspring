package org.example.service;

import org.example.dao.DefaultAccountDao;
import org.example.dao.DefaultCustomerDao;
import org.example.domain.Account;
import org.example.domain.Currency;
import org.example.domain.Customer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultCustomerService implements CustomerService {
    private final DefaultCustomerDao customerDao;

    public DefaultCustomerService(DefaultCustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    @Override
    public Customer save(Customer customer) {
        return customerDao.save(customer);
    }

    @Override
    public boolean delete(Customer customer) {
        return customerDao.delete(customer);
    }

    @Override
    public void deleteAll(List<Customer> customers) {
        customerDao.deleteAll(customers);
    }

    @Override
    public void saveAll(List<Customer> customers) {
        customerDao.saveAll(customers);
    }

    @Override
    public List<Customer> findAll() {
        return customerDao.findAll();
    }

    @Override
    public boolean deleteById(long id) {
        return customerDao.deleteById(id);
    }

    @Override
    public Customer getOne(long id) {
        return customerDao.getOne(id);
    }

    @Override
    public boolean update(long id, Customer customer) {
        return customerDao.update(id, customer);
    }
    @Override
    public boolean createAccount(long id, Currency currency){
        Customer customer = getOne(id);
        if (customer == null){
            return false;
        }
        List<Account> accounts = customer.getAccounts();
        accounts.add(new Account(currency,customer));
        customer.setAccounts(accounts);
        update(id,customer);
        return true;
    }

    @Override
    public boolean deleteAccount(long id, String number){
        Customer customer = getOne(id);
        List<Account> accounts = customer.getAccounts();
        for (int i=0; i<accounts.size(); i++) {
            if(accounts.get(i).getNumber().equals(number)){
                accounts.remove(i);
                return true;
            }
        }
        customer.setAccounts(accounts);
        update(id,customer);
        return false;
    }



}
