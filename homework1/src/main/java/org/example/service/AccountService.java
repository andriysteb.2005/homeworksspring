package org.example.service;

import org.example.domain.Account;

import java.util.List;

public interface AccountService {
    Account save(Account account);
    boolean delete(Account account);
    void deleteAll(List<Account> accounts);
    void saveAll(List<Account> accounts);
    List<Account> findAll();
    boolean deleteById(long id);
    Account getOne(long id);
    public boolean update(long id, Account account);
    public boolean depositeAccount(String number, double sum);
    public boolean withdrawMoney(String number, double sum);
    public boolean transferMoney(String from, String to, double sum);

}
