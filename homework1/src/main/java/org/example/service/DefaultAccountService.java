package org.example.service;

import org.example.dao.DefaultAccountDao;
import org.example.domain.Account;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultAccountService implements AccountService {
    private final DefaultAccountDao accountDao;

    public DefaultAccountService(DefaultAccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public Account save(Account account) {
        return accountDao.save(account);
    }

    @Override
    public boolean delete(Account account) {
        return accountDao.delete(account);
    }

    @Override
    public void deleteAll(List<Account> accounts) {
        accountDao.deleteAll(accounts);
    }

    @Override
    public void saveAll(List<Account> accounts) {
        accountDao.saveAll(accounts);
    }

    @Override
    public List<Account> findAll() {
        return accountDao.findAll();
    }

    @Override
    public boolean deleteById(long id) {
        return accountDao.deleteById(id);
    }

    @Override
    public Account getOne(long id) {
        return accountDao.getOne(id);
    }
    @Override
    public boolean update(long id, Account account) {
        return false;
    }
    @Override
    public boolean depositeAccount(String number, double sum) {
        Account account = accountDao.getOne(number);
        if (account != null) {
            account.setBalance(account.getBalance() + sum);
            update(account.getId(), account);
            return true;
        }
        return false;
    }

    @Override
    public boolean withdrawMoney(String number, double sum) {
        Account account = accountDao.getOne(number);
        if (account != null) {
            if (account.getBalance() >= sum) {
                account.setBalance(account.getBalance() - sum);
                update(account.getId(), account);
                return true;

            }
        }
        return false;
    }

    @Override
    public boolean transferMoney(String from, String to, double sum) {
        if(withdrawMoney(from,sum)){
            depositeAccount(to,sum);
            return true;
        }
        return false;
    }

}
