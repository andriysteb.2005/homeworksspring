package org.example.controller;

import org.example.domain.Account;
import org.example.domain.Currency;
import org.example.domain.Customer;
import org.example.service.DefaultCustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/customers")
public class CustomerController {
    private final DefaultCustomerService customerService;
    public CustomerController(DefaultCustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCustomer(@PathVariable Long id) {
        return ResponseEntity.ok(customerService.getOne(id));
    }

    @GetMapping
    public ResponseEntity<?> getAllCustomers() {
        return ResponseEntity.ok(customerService.findAll());
    }

    @PostMapping
    public void create( String name,String email,  Integer age){
        Customer customer = customerService.save(new Customer( name,  email,  age));
        if(customer != null){
            ResponseEntity.ok().body("Created successfully");
        }
        ResponseEntity.notFound().build();
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> updateCustomer(@PathVariable("id") Long id,  String name, String email, Integer age) {
        Customer existingCustomer = customerService.getOne(id);
        existingCustomer.setName(name);
        existingCustomer.setEmail(email);
        existingCustomer.setAge(age);
        boolean result = customerService.update(id,existingCustomer);
        if(result){
            return ResponseEntity.ok().body("Updated successfully");
        }
        return ResponseEntity.ok().body("Nothing changed");

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable Long id) {
        customerService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/{id}/accounts/create")
    public ResponseEntity<?> createAccount(@PathVariable("id") Long id, Currency currency){
        boolean result = customerService.createAccount(id,currency);
        return result? ResponseEntity.ok().body("Created successfully"):ResponseEntity.notFound().build();
    }
    @DeleteMapping("/{id}/accounts/delete")
    public ResponseEntity<?> deleteAccount(@PathVariable("id") Long id, String number){
        boolean result = customerService.deleteAccount(id,number);
        return result? ResponseEntity.ok().body("Deleted successfully"):ResponseEntity.notFound().build();
    }

}

