package org.example.service;

import org.example.dao.AccountRepository;
import org.example.domain.Account;
import org.example.service.DefaultAccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class DefaultAccountServiceTest {

    @InjectMocks
    private DefaultAccountService accountService;

    @Mock
    private AccountRepository accountRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testWithdrawMoney() {
        Account account = new Account();
        account.setNumber("123456");
        account.setBalance(200.0);

        when(accountRepository.findByNumber("123456")).thenReturn(account);

        boolean result = accountService.withdrawMoney("123456", 50.0);

        assertTrue(result);
        assertEquals(150.0, account.getBalance());
    }

    @Test
    public void testDepositeAccount() {
        Account account = new Account();
        account.setNumber("123456");
        account.setBalance(200.0);

        when(accountRepository.findByNumber("123456")).thenReturn(account);

        boolean result = accountService.depositeAccount("123456", 50.0);

        assertTrue(result);
        assertEquals(250.0, account.getBalance());
    }

    @Test
    public void testTransferMoney() {
        Account fromAccount = new Account();
        fromAccount.setNumber("123456");
        fromAccount.setBalance(200.0);

        Account toAccount = new Account();
        toAccount.setNumber("654321");
        toAccount.setBalance(200.0);

        when(accountRepository.findByNumber("123456")).thenReturn(fromAccount);
        when(accountRepository.findByNumber("654321")).thenReturn(toAccount);

        boolean result = accountService.transferMoney("123456", "654321", 50.0);

        assertTrue(result);
        assertEquals(150.0, fromAccount.getBalance());
        assertEquals(250.0, toAccount.getBalance());
    }
}
