package org.example.service;

import org.example.dao.CustomerEmployerRepository;
import org.example.dao.EmployerRepository;
import org.example.domain.Employer;
import org.example.service.DefaultEmployerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.mockito.Mockito.*;

public class DefaultEmployerServiceTest {

    @InjectMocks
    private DefaultEmployerService employerService;

    @Mock
    private EmployerRepository employerRepository;

    @Mock
    private CustomerEmployerRepository customerEmployerRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testDelete() {
        Employer employer = new Employer();
        employer.setId(1L);

        when(employerRepository.findById(1L)).thenReturn(Optional.of(employer));

        employerService.delete(1L);

        verify(customerEmployerRepository, times(1)).deleteByEmployerId(1L);
        verify(employerRepository, times(1)).delete(employer);
    }
}
