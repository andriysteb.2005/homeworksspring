package org.example.service;

import org.example.dao.AccountRepository;
import org.example.dao.CustomerRepository;
import org.example.domain.Account;
import org.example.domain.Customer;
import org.example.domain.Currency;
import org.example.service.DefaultCustomerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class DefaultCustomerServiceTest {

    @InjectMocks
    private DefaultCustomerService customerService;

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private AccountRepository accountRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    public void testCreateAccount() {
        Customer customer = new Customer();
        customer.setId(1L);

        when(customerRepository.getOne(1L)).thenReturn(customer);
        when(accountRepository.save(any())).thenAnswer(i -> i.getArguments()[0]);

        boolean result = customerService.createAccount(1L, Currency.USD);

        assertTrue(result);
    }
}
