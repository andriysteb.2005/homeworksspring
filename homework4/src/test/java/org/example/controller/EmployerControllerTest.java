package org.example.controller;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.NonNull;
import org.example.domain.Employer;
import org.example.domain.Customer;
import org.example.domain.Role;
import org.example.domain.User;
import org.example.domain.dto.EmployerDtoResponse;
import org.example.domain.dto.CustomerDtoResponse;
import org.example.filter.JwtFilter;
import org.example.service.*;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("local")
@WithMockUser(username = "a", roles = {"USER"})
@WebMvcTest(EmployerController.class)
public class EmployerControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private EmployerService employerService;
    @MockBean
    private  EmployerDtoResponseMapper employerDtoResponseMapper;
    @MockBean
    private  EmployerDtoRequestMapper employerDtoRequestMapper;
    @MockBean
    private PasswordEncoder passwordEncoder;
    @MockBean
    private UserService userService;
    @MockBean
    JwtProvider jwtProvider;
    @MockBean
    private JwtFilter jwtFilter;



    @TestConfiguration
    static class TestConfig{
    @Bean
    public JwtFilter jwtFilter(){
        return new JwtFilter(new JwtProvider("LejjnLZua6SlR7eZXByD2+9M5P+dYxK3IlfA6XgPksuXijiXMAcpulI03o2V",
                "LejjnLZua6SlR7eZXByD2+9M5P+dYxK3IlfA6XgPksuXijiXMAcpulI03o2V"));
    }

    }

    public String generateAccessToken(@NonNull User user) {
        final LocalDateTime now = LocalDateTime.now();
        final Instant accessExpirationInstant = now.plusMinutes(5).atZone(ZoneId.systemDefault()).toInstant();
        final Date accessExpiration = Date.from(accessExpirationInstant);
        return Jwts.builder()
                .setSubject(user.getUserName())
                .setExpiration(accessExpiration)
                .signWith(Keys.hmacShaKeyFor(Decoders.BASE64.decode("LejjnLZua6SlR7eZXByD2+9M5P+dYxK3IlfA6XgPksuXijiXMAcpulI03o2Vq+PjYENhgTJGXLNm7YS4f1+IMw==")))
                .claim("roles", user.getRoles())
                .claim("firstName", user.getUserName())
                .compact();
    }


    @Test
    public void testFindAll() throws Exception {
        User user = new User(101L, "a", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new Role(101L, "USER", new User()))));

        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(user));

        Employer employer = new Employer();
        EmployerDtoResponse employerDtoResponse = new EmployerDtoResponse();
        employerDtoResponse.setName("Tom");
        employerDtoResponse.setId(1L);
        when(employerService.findAll())
                .thenReturn(List.of(employer));
        when(employerDtoResponseMapper.convertToDto(employer)).thenReturn(employerDtoResponse);



        mockMvc.perform(MockMvcRequestBuilders.get("/companies/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+this.generateAccessToken(user)))
                .andExpect(status().isOk());
    }


    @Test
    public void testCreateCompany() throws Exception {
        User user = new User(101L, "a", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new Role(101L, "USER", new User()))));
        Employer employer = new Employer();
        EmployerDtoResponse employerDtoResponse = new EmployerDtoResponse();
        employerDtoResponse.setName("APPLE");
        employerDtoResponse.setAddress("LVIV");
        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(user));

        when(employerService.save(any())).thenReturn(employer);

        when(employerDtoResponseMapper.convertToDto(employer)).thenReturn(employerDtoResponse);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/companies/")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+this.generateAccessToken(user))
                        .content(
                                """
                                        {
                                            "name": "APPLE",
                                            "address": "LVIV"
                                        }
                                        """
                        ))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteCustomer() throws Exception {
        User user = new User(101L, "a", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new Role(101L, "USER", new User()))));
        Employer employer = new Employer();

        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(user));
        when(employerService.getOne(1)).thenReturn(employer);


        this.mockMvc.perform(MockMvcRequestBuilders.delete("/employers/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer " + this.generateAccessToken(user)))
                .andExpect(status().isOk());
    }
}


