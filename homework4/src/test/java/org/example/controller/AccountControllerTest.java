package org.example.controller;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import io.micrometer.common.lang.NonNull;
import org.example.domain.Account;
import org.example.domain.Role;
import org.example.domain.User;
import org.example.domain.dto.AccountDtoResponse;
import org.example.filter.JwtFilter;
import org.example.service.*;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.filter.GenericFilterBean;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("local")
@WithMockUser(username = "a", roles = {"USER"})
@WebMvcTest(AccountController.class)
public class AccountControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private AccountDtoResponseMapper accountDtoResponseMapper;
    @MockBean
    private AccountDtoRequestMapper accountDtoRequestMapper;
    @MockBean
    private AccountService accountService;
    @MockBean
    private PasswordEncoder passwordEncoder;
    @MockBean
    private UserService userService;
    @MockBean
    JwtProvider jwtProvider;
    @MockBean
    private JwtFilter jwtFilter;



    @TestConfiguration
    static class TestConfig {
        @Bean
        public JwtFilter jwtFilter() {
            return new JwtFilter(new JwtProvider("LejjnLZua6SlR7eZXByD2+9M5P+dYxK3IlfA6XgPksuXijiXMAcpulI03o2V",
                    "LejjnLZua6SlR7eZXByD2+9M5P+dYxK3IlfA6XgPksuXijiXMAcpulI03o2V"));
        }

    }

    public String generateAccessToken(@NonNull User user) {
        final LocalDateTime now = LocalDateTime.now();
        final Instant accessExpirationInstant = now.plusMinutes(5).atZone(ZoneId.systemDefault()).toInstant();
        final Date accessExpiration = Date.from(accessExpirationInstant);
        return Jwts.builder()
                .setSubject(user.getUserName())
                .setExpiration(accessExpiration)
                .signWith(Keys.hmacShaKeyFor(Decoders.BASE64.decode("LejjnLZua6SlR7eZXByD2+9M5P+dYxK3IlfA6XgPksuXijiXMAcpulI03o2Vq+PjYENhgTJGXLNm7YS4f1+IMw==")))
                .claim("roles", user.getRoles())
                .claim("firstName", user.getUserName())
                .compact();
    }

    @Test
    public void testFindAll() throws Exception {
        User user = new User(101L, "a", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new Role(101L, "USER", new User()))));

        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(user));

        Account account = new Account();
        AccountDtoResponse accountDtoResponse = new AccountDtoResponse();
        accountDtoResponse.setNumber("b4tt");
        accountDtoResponse.setId(1L);
        when(accountService.findAll())
                .thenReturn(List.of(account));
        when(accountDtoResponseMapper.convertToDto(account)).thenReturn(accountDtoResponse);


        mockMvc.perform(MockMvcRequestBuilders.get("/accounts/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer " + this.generateAccessToken(user)))
                .andExpect(status().isOk());
    }

    @Test
    public void testWithdrawMoney() throws Exception {
        User user = new User(101L, "a", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new Role(101L, "USER", new User()))));
        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(user));

        when(accountService.withdrawMoney("3535tf4", 100)).thenReturn(true);
        mockMvc.perform(MockMvcRequestBuilders.put("/accounts/withdraw")
                        .with(csrf())
                        .param("number", "3535tf4")
                        .param("sum", "100")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer " + this.generateAccessToken(user)))
                .andExpect(status().isOk());
    }

    @Test
    public void testDepositeMoney() throws Exception {
        User user = new User(101L, "a", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new Role(101L, "USER", new User()))));

        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(user));

        when(accountService.depositeAccount("3535tf4", 100)).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders.put("/accounts/refill")
                        .with(csrf())
                        .param("number", "3535tf4")
                        .param("sum", "100")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer " + this.generateAccessToken(user)))
                .andExpect(status().isOk());
    }

    @Test
    public void testTransferMoney() throws Exception {
        User user = new User(101L, "a", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new Role(101L, "USER", new User()))));

        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(user));

        when(accountService.transferMoney("3535tf4", "rgv56t", 100)).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders.put("/accounts/transfer")
                        .with(csrf())
                        .param("numberFrom", "3535tf4")
                        .param("numberTo", "rgv56t")
                        .param("sum", "100")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer " + this.generateAccessToken(user)))
                .andExpect(status().isOk());
    }
}

