package org.example.controller;

import jakarta.validation.constraints.Min;
import org.example.domain.Employer;
import org.example.domain.dto.EmployerDtoRequest;
import org.example.domain.dto.EmployerDtoResponse;
import org.example.service.EmployerDtoRequestMapper;
import org.example.service.EmployerDtoResponseMapper;
import org.example.service.EmployerService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Validated
@RestController
@RequestMapping("/employers")
public class EmployerController {
    private final EmployerDtoResponseMapper employerDtoResponseMapper;
    private final EmployerDtoRequestMapper employerDtoRequestMapper;
    private final EmployerService employerService;

    public EmployerController(EmployerDtoResponseMapper employerDtoResponseMapper, EmployerDtoRequestMapper employerDtoRequestMapper, EmployerService employerService) {
        this.employerDtoResponseMapper = employerDtoResponseMapper;
        this.employerDtoRequestMapper = employerDtoRequestMapper;
        this.employerService = employerService;
    }

    @GetMapping("/allEmployers")
    public ResponseEntity<?> findAll() {
        List<Employer> employers = employerService.findAll();
        List<EmployerDtoResponse> dtos = employers.stream().map(employerDtoResponseMapper::convertToDto).toList();
        return ResponseEntity.ok(dtos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id")@Min(0) Long id) {
        Employer employer = employerService.getOne(id);
        EmployerDtoResponse dto = employerDtoResponseMapper.convertToDto(employer);
        if (employer != null) {
            return ResponseEntity.ok(dto);
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping()
    public ResponseEntity<EmployerDtoResponse> create(@RequestBody @Validated EmployerDtoRequest employerDtoRequest) {
        Employer employer =employerService.save(employerDtoRequestMapper.convertToEntity(employerDtoRequest));
        return ResponseEntity.ok().body(employerDtoResponseMapper.convertToDto(employer));

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id")@Min(0) Long id) {
        employerService.delete(id);
        return ResponseEntity.ok().body("Deleted successfully");
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@PathVariable("id")@Min(0) Long id, @RequestBody @Validated EmployerDtoRequest employerDtoRequest) {
        Employer employer = employerDtoRequestMapper.convertToEntity(employerDtoRequest);
        employer.setId(id);
        boolean result = employerService.update(employer);
        if (result) {
            return ResponseEntity.ok().body("Changed successfully");
        }
        return ResponseEntity.ok().body("Nothing changed");
    }



}
