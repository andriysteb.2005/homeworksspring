package org.example.controller;


import jakarta.validation.constraints.Min;
import org.example.domain.Currency;
import org.example.domain.Customer;
import org.example.domain.dto.CustomerDtoRequest;
import org.example.domain.dto.CustomerDtoResponse;
import org.example.service.CustomerDtoRequestMapper;
import org.example.service.CustomerDtoResponseMapper;
import org.example.service.CustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/customers")
public class CustomerController {
    private CustomerDtoRequestMapper customerDtoRequestMapper;
    private CustomerDtoResponseMapper customerDtoResponseMapper;
    private CustomerService customerService;

    public CustomerController(CustomerDtoRequestMapper customerDtoRequestMapper, CustomerDtoResponseMapper customerDtoResponseMapper, CustomerService customerService) {
        this.customerDtoRequestMapper = customerDtoRequestMapper;
        this.customerDtoResponseMapper = customerDtoResponseMapper;
        this.customerService = customerService;
    }


    @GetMapping("/allCustomers")
    public ResponseEntity<?> findAll(@RequestParam(defaultValue = "1") @Min(1) Integer page, @RequestParam(defaultValue = "10")@Min(0) Integer size){
        List<Customer> companies = customerService.findAll(page, size);
        List<CustomerDtoResponse> dtos = companies.stream().map(customerDtoResponseMapper::convertToDto).toList();
        return ResponseEntity.ok(dtos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id")@Min(0) Long id){
        Customer customer = customerService.getOne(id);
        CustomerDtoResponse dto = customerDtoResponseMapper.convertToDto(customer);
        if (customer != null){
            return ResponseEntity.ok(dto);
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/")
    public ResponseEntity<CustomerDtoResponse> create(@RequestBody @Validated CustomerDtoRequest customerDtoRequest){
        Customer  customer = customerService.save(customerDtoRequestMapper.convertToEntity(customerDtoRequest));
        return ResponseEntity.ok().body(customerDtoResponseMapper.convertToDto(customer));
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") @Min(0) Long id){
        boolean result = customerService.delete(customerService.getOne(id));
        return result? ResponseEntity.ok().body("Deleted successfully"):ResponseEntity.notFound().build();
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@PathVariable("id")@Min(0) Long id, @RequestBody @Validated CustomerDtoRequest customerDtoRequest){
        Customer customer = customerDtoRequestMapper.convertToEntity(customerDtoRequest);
        customer.setId(id);
        boolean result = customerService.update(customer);
        if (result) {
            return ResponseEntity.ok().body("Changed successfully");
        }
        return ResponseEntity.ok().body("Nothing changed");
    }

    @PostMapping("/{id}/accounts/create")
    public ResponseEntity<?> createAccount(@PathVariable("id")@Min(0) Long id,Currency currency){
        boolean result = customerService.createAccount(id,currency);
        return result? ResponseEntity.ok().body("Created successfully"):ResponseEntity.notFound().build();
    }
    @DeleteMapping("/{id}/accounts/delete")
    public ResponseEntity<?> deleteAccount(@PathVariable("id")@Min(0) Long id, String number){
        boolean result = customerService.deleteAccount(id,number);
        if(result){
            return ResponseEntity.ok().body("Deleted successfully");
        }
        return ResponseEntity.ok().body("Nothing changed");
    }



}

