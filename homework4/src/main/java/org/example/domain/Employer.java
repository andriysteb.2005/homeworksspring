package org.example.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.*;

import java.util.List;
@Entity
@Table(name = "employers")
@Getter
@Setter
@NoArgsConstructor
@ToString(exclude = {"customers"})
@EqualsAndHashCode(of={"id"})
public class Employer extends AbstractEntity {
    private String name;
    private String address;
    @JsonIgnore
    @ManyToMany(mappedBy = "employers")
    private List<Customer> customers;
}