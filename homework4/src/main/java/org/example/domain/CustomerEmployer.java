package org.example.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@ToString
@Getter
@Setter
@Entity
@Table(name = "customer_employer")
public class CustomerEmployer extends AbstractEntity {
    @Column(name = "customer_id")
    private Long customerId;
    @Column(name = "employer_id")
    private Long employerId;
}