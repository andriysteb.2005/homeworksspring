package org.example.dao;

import org.example.domain.CustomerEmployer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface CustomerEmployerRepository extends JpaRepository<CustomerEmployer,Long> {
    @Query("DELETE FROM CustomerEmployer ce WHERE ce.employerId = :id")
    @Modifying
    void deleteByEmployerId(Long id);
}


