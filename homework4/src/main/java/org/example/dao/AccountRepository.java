package org.example.dao;

import org.example.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Long> {
    Account findByNumber(String number);

    void deleteAccountByNumberAndCustomerId(String number, long id);
}
