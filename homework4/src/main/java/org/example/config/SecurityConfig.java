package org.example.config;

import jakarta.servlet.http.HttpServletResponse;
import org.example.filter.JwtFilter;
import lombok.RequiredArgsConstructor;
import org.example.util.JwtUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig {
    private final JwtFilter jwtFilter;
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .authorizeHttpRequests((requests) ->
                                requests
                                        .requestMatchers("/api/auth/login", "/auth/token", "/css/**", "/js/**", "/login", "/registration", "/new", "http://localhost:9000/h2-console" ).permitAll()
                                        .requestMatchers("/customers/**", "/employers/**").hasAnyAuthority(JwtUtils.Roles.USER.name())
                                        .requestMatchers("/**").hasAnyAuthority(JwtUtils.Roles.ADMIN.name())
                                        .anyRequest().authenticated()
                        //.anyRequest().permitAll()
                )
                .rememberMe()
                .tokenValiditySeconds(86400) // 24h // 7d default
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(
                        (request, response, ex) -> {
                            response.sendError(
                                    HttpServletResponse.SC_UNAUTHORIZED,
                                    ex.getMessage()
                            );
                        }
                )
                .and()
                .logout()
                .logoutSuccessUrl("/")
                .invalidateHttpSession(true)
//                    .deleteCookies("JSESSIONID")
                .permitAll()
                .and()
                .addFilterAfter(jwtFilter, UsernamePasswordAuthenticationFilter.class);
        return http.build();
    }
}
