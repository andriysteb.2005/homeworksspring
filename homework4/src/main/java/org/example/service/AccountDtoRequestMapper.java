package org.example.service;

import org.example.domain.Account;
import org.example.domain.dto.AccountDtoRequest;
import org.springframework.stereotype.Service;

@Service
public class AccountDtoRequestMapper extends DtoMapperFacade<Account, AccountDtoRequest> {
    public AccountDtoRequestMapper() {
        super(Account.class, AccountDtoRequest.class);
    }

    @Override
    protected void decorateEntity(Account entity, AccountDtoRequest dto){
        entity.setCurrency(dto.getCurrency());
        entity.setBalance(dto.getBalance());
    }
}

