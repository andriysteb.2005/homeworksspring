package org.example.service;

import org.example.domain.Customer;
import org.example.domain.dto.CustomerDtoRequest;
import org.springframework.stereotype.Service;

@Service
public class CustomerDtoRequestMapper extends DtoMapperFacade<Customer, CustomerDtoRequest>{
    public CustomerDtoRequestMapper() {
        super(Customer.class, CustomerDtoRequest.class);
    }
    @Override
    protected void decorateEntity(Customer entity, CustomerDtoRequest dto) {
        entity.setName(dto.getName());
        entity.setAge(dto.getAge());
        entity.setEmail(dto.getEmail());
        entity.setPhoneNum(dto.getPhone());
        entity.setPassword(dto.getPassword());
    }
}

