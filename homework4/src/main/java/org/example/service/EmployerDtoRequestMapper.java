package org.example.service;

import org.example.domain.Employer;
import org.example.domain.dto.EmployerDtoRequest;
import org.springframework.stereotype.Service;

@Service
public class EmployerDtoRequestMapper extends DtoMapperFacade<Employer, EmployerDtoRequest> {
    public EmployerDtoRequestMapper() {
        super(Employer.class, EmployerDtoRequest.class);
    }
    @Override
    protected void decorateEntity(Employer entity, EmployerDtoRequest dto){
        entity.setAddress(dto.getAddress());
        entity.setName(dto.getName());
    }
}

