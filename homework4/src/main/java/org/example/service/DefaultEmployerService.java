package org.example.service;

import jakarta.transaction.Transactional;
import org.example.dao.CustomerEmployerRepository;
import org.example.dao.EmployerRepository;
import org.example.domain.Employer;
import org.springframework.stereotype.Service;

import java.util.List;
@Transactional
@Service
public class DefaultEmployerService implements EmployerService {
    private EmployerRepository employerRepository;
    private CustomerEmployerRepository customerEmployerRepository;

    public DefaultEmployerService(EmployerRepository employerRepository, CustomerEmployerRepository customerEmployerRepository) {
        this.employerRepository = employerRepository;
        this.customerEmployerRepository = customerEmployerRepository;
    }

    @Override
    public Employer save(Employer employer) {
        return employerRepository.save(employer);
    }


    @Override
    public void delete(Long id) {
        customerEmployerRepository.deleteByEmployerId(id);
        employerRepository.delete(employerRepository.findById(id).get());

    }

    @Override
    public void deleteAll(List<Employer> employers) {
        employerRepository.deleteAll(employers);
    }

    @Override
    public void saveAll(List<Employer> employers) {
        employerRepository.saveAll(employers);
    }

    @Override
    public List<Employer> findAll() {
        return employerRepository.findAll();
    }

    @Override
    public Employer getOne(long id) {
        return employerRepository.getOne(id);
    }

    @Override
    public boolean update(Employer employer){
        employerRepository.save(employer);
        return true;

    }
}
