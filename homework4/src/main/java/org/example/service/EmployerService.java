package org.example.service;

import org.example.domain.Employer;

import java.util.List;

public interface EmployerService {
    Employer save(Employer employer);
    void delete(Long id);
    void deleteAll(List<Employer> employers);
    void saveAll(List<Employer> employers);
    List<Employer> findAll();
    Employer getOne(long id);
    boolean update(Employer employer);

}
