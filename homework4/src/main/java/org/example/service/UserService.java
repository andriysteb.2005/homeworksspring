package org.example.service;



import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.example.dao.UserRepository;
import org.example.domain.User;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public Optional<User> getByLogin(@NonNull String login) {

        return userRepository.findUsersByUserName(login);
    }

}