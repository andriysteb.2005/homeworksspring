package org.example.service;

import jakarta.transaction.Transactional;
import org.example.dao.AccountRepository;
import org.example.dao.CustomerRepository;
import org.example.domain.Account;
import org.example.domain.Currency;
import org.example.domain.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Transactional
@Service
public class DefaultCustomerService implements CustomerService {
    private CustomerRepository customerRepository;
    private AccountRepository accountRepository;

    public DefaultCustomerService(CustomerRepository customerRepository, AccountRepository accountRepository) {
        this.customerRepository = customerRepository;
        this.accountRepository = accountRepository;
    }

    @Override
    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public boolean delete(Customer customer) {
        customerRepository.delete(customer);
        return true;

    }

    @Override
    public void deleteAll(List<Customer> customers) {
        customerRepository.deleteAll(customers);
    }

    @Override
    public void saveAll(List<Customer> customers) {
        customerRepository.saveAll(customers);
    }

    @Override
    public List<Customer> findAll(Integer page, Integer size) {
        Sort sort = Sort.by(new Sort.Order(Sort.Direction.ASC, "id"));
        Pageable pageable = (Pageable) PageRequest.of(page-1, size, sort);
        Page<Customer> customerPage = customerRepository.findAll(pageable);
        return customerPage.toList();

    }



    @Override
    public Customer getOne(long id) {
        return customerRepository.getOne(id);
    }

    @Override
    public boolean update(Customer customer) {
        customerRepository.save(customer);
        return true;
    }
    @Override
    public boolean createAccount(long id, Currency currency){
        Customer customer = getOne(id);
        if (customer == null){
            return false;
        }
        Account account = new Account(currency,customer);
        account.setBalance(0.0);
        accountRepository.save(account);
        return true;

    }

    @Override
    public boolean deleteAccount(long id, String number){
        accountRepository.deleteAccountByNumberAndCustomerId(number,id);
        return true;


    }



}
