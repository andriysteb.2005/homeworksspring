package org.example.service;

import org.example.domain.Account;

import java.util.List;

public interface AccountService {
    Account save(Account account);
    void delete(Account account);
    void deleteAll(List<Account> accounts);
    void saveAll(List<Account> accounts);
    List<Account> findAll();
    Account getOne(long id);
    Account getOne(String number);
     boolean update(Account account);
     boolean depositeAccount(String number, double sum);
     boolean withdrawMoney(String number, double sum);
     boolean transferMoney(String from, String to, double sum);

}
