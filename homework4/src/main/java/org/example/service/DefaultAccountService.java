package org.example.service;

import org.example.dao.AccountRepository;
import org.example.domain.Account;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultAccountService implements AccountService {
    private AccountRepository accountRepository;

    public DefaultAccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }


    @Override
    public Account save(Account account) {
        return accountRepository.save(account);
    }

    @Override
    public void delete(Account account) { accountRepository.delete(account); }

    @Override
    public void deleteAll(List<Account> accounts) {
        accountRepository.deleteAll(accounts);
    }

    @Override
    public void saveAll(List<Account> accounts) {
        accountRepository.saveAll(accounts);
    }

    @Override
    public List<Account> findAll() {
        return accountRepository.findAll();
    }


    @Override
    public Account getOne(long id) {
        return accountRepository.getOne(id);
    }
    @Override
    public Account getOne(String number) { return accountRepository.findByNumber(number);
    }



    @Override
    public boolean update(Account account) {
        return false;
    }
    @Override
    public boolean depositeAccount(String number, double sum) {
        Account account = getOne(number);
        if (account != null) {
            account.setBalance(account.getBalance() + sum);
            update(account);
            return true;
        }
        return false;
    }

    @Override
    public boolean withdrawMoney(String number, double sum) {
        Account account = getOne(number);
        if (account != null) {
            if (account.getBalance() >= sum) {
                account.setBalance(account.getBalance() - sum);
                update(account);
                return true;

            }
        }
        return false;
    }

    @Override
    public boolean transferMoney(String from, String to, double sum) {
        if(withdrawMoney(from,sum)){
            depositeAccount(to,sum);
            return true;
        }
        return false;
    }

}
