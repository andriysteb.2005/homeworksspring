DROP TABLE IF EXISTS customers CASCADE;
CREATE TABLE customers
(
    id                 SERIAL PRIMARY KEY,
    name               VARCHAR(250) NOT NULL,
    password           VARCHAR(250) NOT NULL,
    email              VARCHAR(250) NOT NULL,
    phone_num              VARCHAR(250) NOT NULL,
    age                INT          NOT NULL,
    created_date      TIMESTAMP,
    last_modified_date TIMESTAMP

);

DROP TABLE IF EXISTS accounts CASCADE;
CREATE TABLE accounts
(
    id                 SERIAL PRIMARY KEY,
    number             VARCHAR(250) NOT NULL,
    currency           VARCHAR(3)   NOT NULL,
    balance            DOUBLE PRECISION DEFAULT 0,
    customer_id        INTEGER REFERENCES customers (id),
    created_date      TIMESTAMP,
    last_modified_date TIMESTAMP


);


DROP TABLE IF EXISTS employers CASCADE;
CREATE TABLE employers
(
    id                 SERIAL PRIMARY KEY,
    name               VARCHAR(250) NOT NULL,
    address            VARCHAR(250) NOT NULL,
    created_date      TIMESTAMP,
    last_modified_date TIMESTAMP

);

DROP TABLE IF EXISTS customer_employer CASCADE;
CREATE TABLE customer_employer
(
    id                 SERIAL PRIMARY KEY,
    customer_id        INTEGER REFERENCES customers (id) NOT NULL,
    employer_id        INTEGER REFERENCES employers (id) NOT NULL,
    created_date      TIMESTAMP,
    last_modified_date TIMESTAMP

);



