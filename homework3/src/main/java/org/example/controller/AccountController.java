package org.example.controller;


import jakarta.validation.constraints.Min;
import org.example.domain.Account;
import org.example.domain.dto.AccountDtoResponse;
import org.example.service.AccountDtoRequestMapper;
import org.example.service.AccountDtoResponseMapper;
import org.example.service.AccountService;
import org.example.service.DefaultAccountService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Validated
@RestController
@RequestMapping("/accounts")
public class AccountController {
    private AccountDtoResponseMapper accountDtoResponseMapper;
    private AccountDtoRequestMapper accountDtoRequestMapper;
    private AccountService accountService;

    public AccountController(AccountDtoResponseMapper accountDtoResponseMapper, AccountDtoRequestMapper accountDtoRequestMapper, AccountService accountService) {
        this.accountDtoResponseMapper = accountDtoResponseMapper;
        this.accountDtoRequestMapper = accountDtoRequestMapper;
        this.accountService = accountService;
    }

    @GetMapping("/allAccounts")
    public ResponseEntity<?> findAll() {
        List<Account> companies = accountService.findAll();
        List<AccountDtoResponse> dtos = companies.stream().map(accountDtoResponseMapper::convertToDto).toList();
        return ResponseEntity.ok(dtos);
    }

    @PutMapping("/withdraw")
    public ResponseEntity<?> withdraw(String number,@Min(0) double sum) {
        boolean result = accountService.withdrawMoney(number, sum);
        if (result) {
            return ResponseEntity.ok().body("Money were withdrawn successfully");
        }
        return ResponseEntity.ok().body("Nothing changed");
    }

    @PutMapping("/deposite")
    public ResponseEntity<?> deposite(String number,@Min(0) double sum) {
        boolean result = accountService.depositeAccount(number, sum);
        if (result) {
            return ResponseEntity.ok().body("+Money");
        }
        return ResponseEntity.ok().body("Nothing changed");
    }

    @PutMapping("/transfer")
    public ResponseEntity<?> transfer(String numberFrom, String numberTo,@Min(0) double sum) {
        boolean result = accountService.transferMoney(numberFrom, numberTo, sum);
        if (result) {
            return ResponseEntity.ok().body("Money were transferred successfully");
        }
        return ResponseEntity.ok().body("Nothing changed");
    }




}
