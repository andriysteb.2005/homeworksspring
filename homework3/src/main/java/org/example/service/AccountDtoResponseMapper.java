package org.example.service;

import org.example.domain.Account;
import org.example.domain.dto.AccountDtoResponse;
import org.springframework.stereotype.Service;

@Service
public class AccountDtoResponseMapper extends DtoMapperFacade<Account, AccountDtoResponse> {
    public AccountDtoResponseMapper() {
        super(Account.class, AccountDtoResponse.class);
    }

    @Override
    protected void decorateDto(AccountDtoResponse dto, Account entity){
        dto.setBalance(entity.getBalance());
        dto.setCurrency(entity.getCurrency());
        dto.setNumber(entity.getNumber());
        dto.setId(entity.getId());
    }
}

