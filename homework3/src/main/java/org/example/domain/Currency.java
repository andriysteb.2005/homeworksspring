package org.example.domain;

public enum Currency {
    USD,
    EUR,
    UAH,
    CHF,
    GBP
}
