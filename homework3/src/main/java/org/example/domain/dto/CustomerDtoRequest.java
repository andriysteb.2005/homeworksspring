package org.example.domain.dto;

import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDtoRequest {
    @NotNull
    @Size(min = 2, message = "Name must have at least 2 characters")
    private String name;

    @NotNull
    @Email(message = "Email is incorrect")
    private String email;

    @NotNull
    @Min(value = 18, message = "Age must be 18+")
    private Integer age;

    @Pattern(regexp = "(\\+38|0)[0-9]{9}")
    @NotBlank
    private String phoneNum;
    private String password;

}
