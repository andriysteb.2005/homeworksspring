package org.example.dao;

import org.example.domain.Account;
import org.example.domain.Currency;
import org.example.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {
    Account findByNumber(String number);

    void deleteAccountByNumberAndCustomerId(String number, long id);
}
