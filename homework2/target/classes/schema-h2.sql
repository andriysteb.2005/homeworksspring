
DROP TABLE IF EXISTS customers CASCADE;
DROP TABLE IF EXISTS employers CASCADE;
DROP TABLE IF EXISTS accounts CASCADE;

CREATE TABLE customers
(
    id INT AUTO_INCREMENT  PRIMARY KEY,
    name VARCHAR(250) NOT NULL,
    email VARCHAR(250) NOT NULL,
    age INT NOT NULL

);

CREATE TABLE employers
(
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(250) NOT NULL,
    address VARCHAR(250) NOT NULL

);

CREATE TABLE accounts
(
    id INT AUTO_INCREMENT  PRIMARY KEY,
    number VARCHAR(250) NOT NULL,
    currency VARCHAR(3) NOT NULL,
    balance DOUBLE PRECISION  DEFAULT 0,
    customer_id INTEGER REFERENCES customers(id)

);

CREATE TABLE customer_employer
(
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    customer_id INTEGER REFERENCES customers (id) NOT NULL,
    employer_id INTEGER REFERENCES employers (id) NOT NULL

);


