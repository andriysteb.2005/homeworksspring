
INSERT INTO employers (name, address) VALUES ('Employer1', 'Paris');
INSERT INTO employers (name, address) VALUES ('Employer2', 'London');

INSERT INTO customers (name, email, age) VALUES ('Customer1', 'email1@e.com', 30);
INSERT INTO customers (name, email, age) VALUES ('Customer2', 'email2@e.com', 25);


INSERT INTO accounts (number, currency, balance, customer_id) VALUES ('1234567890', 'UAH', 1000.00, 1);
INSERT INTO accounts (number, currency, balance, customer_id) VALUES ('0987654321', 'USD', 500.00, 2);

INSERT INTO customer_employer (customer_id, employer_id) VALUES (1, 1);
INSERT INTO customer_employer (customer_id, employer_id) VALUES (2, 2);
