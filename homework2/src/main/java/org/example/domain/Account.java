package org.example.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.UUID;
@NoArgsConstructor
//@EqualsAndHashCode(callSuper = true)
@Getter
@ToString
@Setter
@Table(name="accounts")
@Entity
public class Account extends AbstractEntity{
    @Column(name = "number")
    private String number;
    @Column(name = "currency")
    @Enumerated(EnumType.STRING)
    private Currency currency;
    @Column(name = "balance")
    private Double balance;


    @ManyToOne
    @JsonIgnore
    private Customer customer;

    public Account(Currency currency, Customer customer) {
        this.currency = currency;
        this.customer = customer;
        setNumber(UUID.randomUUID().toString());
    }

}

