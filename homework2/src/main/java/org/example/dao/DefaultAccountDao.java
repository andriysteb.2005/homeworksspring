package org.example.dao;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.PersistenceUnit;
import org.example.domain.Account;
import org.example.domain.Currency;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class DefaultAccountDao implements AccountDao {
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;


    @Override
    public Account save(Account account) {
        account.setId(null);
        EntityTransaction transaction = null;
        try(EntityManager entityManager = entityManagerFactory.createEntityManager()){
            transaction = entityManager.getTransaction();
            transaction.begin();
            entityManager.persist(account);
            transaction.commit();
        } catch (HibernateException e){
            if (transaction != null){
                transaction.rollback();
            }
        }
        return account;

    }

    @Override
    public boolean delete(Account account) {
        EntityTransaction transaction = null;
        try(EntityManager entityManager = entityManagerFactory.createEntityManager()){
            transaction = entityManager.getTransaction();
            transaction.begin();
            entityManager.remove(entityManager.find(Account.class, account.getId()));
            transaction.commit();
        } catch (HibernateException e){
            if (transaction != null){
                transaction.rollback();
                return false;
            }
        }
        return true;

    }

    @Override
    public void deleteAll(List<Account> accounts) {
        EntityTransaction transaction = null;
        try (EntityManager entityManager = entityManagerFactory.createEntityManager()) {
            transaction = entityManager.getTransaction();
            transaction.begin();
            for (Account account : accounts) {
                entityManager.remove(entityManager.find(Account.class, account.getId()));
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }


    }

    @Override
    public void saveAll(List<Account> accounts) {
        EntityTransaction transaction = null;
        try(EntityManager entityManager = entityManagerFactory.createEntityManager()){
            transaction = entityManager.getTransaction();
            transaction.begin();
            for (Account account : accounts) {
                entityManager.persist(account);
            }
            transaction.commit();
        } catch (HibernateException e){
            if (transaction != null){
                transaction.rollback();
            }
        }


    }

    @Override
    public List<Account> findAll(){
        try(EntityManager entityManager = entityManagerFactory.createEntityManager()){
        return  entityManager.createQuery("from Account a", Account.class).getResultList();
    }

}


    @Override
    public Account getOne(long id) {
        try (EntityManager entityManager = entityManagerFactory.createEntityManager()) {
            return entityManager.find(Account.class, id);
        }
    }
    @Override
    public Account getOne(String number){
        try(EntityManager entityManager = entityManagerFactory.createEntityManager()){
            return  entityManager.createQuery("from Account a where a.number = :number", Account.class)
                    .setParameter("number",number).getSingleResult();
        }

    }

    @Override
    public boolean update(Account account){
        EntityTransaction transaction = null;
        try(EntityManager entityManager = entityManagerFactory.createEntityManager()){
            transaction = entityManager.getTransaction();
            transaction.begin();
            entityManager.merge(account);
            transaction.commit();
        } catch (HibernateException e){
            if (transaction != null){
                transaction.rollback();
                return false;
            }
        }
        return true;

    }

}
