package org.example.dao;

import org.example.domain.Account;
import org.example.domain.Customer;

import java.util.List;

public interface AccountDao {
    Account save(Account account);
    boolean delete(Account account);
    void deleteAll(List<Account> accounts);
    void saveAll(List<Account> accounts);
    List<Account> findAll();
    Account getOne(long id);
    public Account getOne(String number);
    public boolean update(Account account);

}
