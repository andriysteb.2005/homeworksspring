package org.example.dao;

import org.example.domain.Employer;

import java.util.List;

public interface EmployerDao {

    void deleteAll(List<Employer> employers);

    boolean delete(Employer employer);
    Employer save(Employer employer);
    void saveAll(List<Employer> employers);
    List<Employer> findAll();
    Employer getOne(long id);
    boolean update(Employer employer);
    void deleteEmployerCustomers(Employer employer);
}
