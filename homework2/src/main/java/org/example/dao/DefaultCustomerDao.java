package org.example.dao;


import jakarta.persistence.*;
import org.example.domain.Account;
import org.example.domain.Customer;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import java.util.List;
@Repository
public class DefaultCustomerDao implements CustomerDao{
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;



    @Override
    public Customer save(Customer customer) {
        customer.setId(null);
        EntityTransaction transaction = null;
        try(EntityManager entityManager = entityManagerFactory.createEntityManager()){
            transaction = entityManager.getTransaction();
            transaction.begin();
            entityManager.persist(customer);
            transaction.commit();
        } catch (HibernateException e){
            if (transaction != null){
                transaction.rollback();
            }
        }
        return customer;

    }

    @Override
    public boolean delete(Customer customer) {
        EntityTransaction transaction = null;
        try(EntityManager entityManager = entityManagerFactory.createEntityManager()){
            transaction = entityManager.getTransaction();
            transaction.begin();
            entityManager.remove(entityManager.find(Customer.class, customer.getId()));
            transaction.commit();
        } catch (HibernateException e){
            if (transaction != null){
                transaction.rollback();
                return false;
            }
        }
        return true;

    }

    @Override
    public void deleteAll(List<Customer> customers) {
        EntityTransaction transaction = null;
        try (EntityManager entityManager = entityManagerFactory.createEntityManager()) {
            transaction = entityManager.getTransaction();
            transaction.begin();
            for (Customer customer : customers) {
                entityManager.remove(entityManager.find(Account.class, customer.getId()));
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }

    }

    @Override
    public void saveAll(List<Customer> customers) {
        EntityTransaction transaction = null;
        try(EntityManager entityManager = entityManagerFactory.createEntityManager()){
            transaction = entityManager.getTransaction();
            transaction.begin();
            for (Customer customer : customers) {
                entityManager.persist(customer);
            }
            transaction.commit();
        } catch (HibernateException e){
            if (transaction != null){
                transaction.rollback();
            }
        }
    }

    @Override
    public List<Customer> findAll() {
        try(EntityManager entityManager = entityManagerFactory.createEntityManager()){
            return  entityManager.createQuery("from Customer c", Customer.class).getResultList();
        }
    }


    @Override
    public Customer getOne(long id) {
        try (EntityManager entityManager = entityManagerFactory.createEntityManager()) {
            EntityGraph<?> entityGraph = entityManager.getEntityGraph("customerWithAccounts");
            return entityManager.createQuery("FROM Customer c WHERE c.id = :id", Customer.class)
                    .setParameter("id", id)
                    .setHint("jakarta.persistence.fetchgraph", entityGraph)
                    .getSingleResult();
        }

    }

    @Override
    public boolean update(Customer customer){
        EntityTransaction transaction = null;
        try(EntityManager entityManager = entityManagerFactory.createEntityManager()){
            transaction = entityManager.getTransaction();
            transaction.begin();
            entityManager.merge(customer);
            transaction.commit();
        } catch (HibernateException e){
            if (transaction != null){
                transaction.rollback();
                return false;
            }
        }
        return true;

    }

}
