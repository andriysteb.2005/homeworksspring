package org.example.dao;

import org.example.domain.Account;
import org.example.domain.Currency;
import org.example.domain.Customer;

import java.util.List;

public interface CustomerDao {
    Customer save(Customer customer);
    boolean delete(Customer customer);
    void deleteAll(List<Customer> customers);
    void saveAll(List<Customer> customers);
    List<Customer> findAll();
    Customer getOne(long id);
    public boolean update(Customer customer);
}
