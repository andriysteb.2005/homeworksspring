package org.example.dao;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.PersistenceUnit;
import org.example.domain.Employer;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import java.util.List;
@Repository
public class DefaultEmployerDao implements EmployerDao{
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;
    @Override
    public Employer save(Employer employer) {
        employer.setId(null);
        EntityTransaction transaction = null;
        try (EntityManager entityManager = entityManagerFactory.createEntityManager()) {
            transaction = entityManager.getTransaction();
            transaction.begin();
            entityManager.persist(employer);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return employer;

    }

    @Override
    public boolean delete(Employer employer) {
        if(employer == null){
            return false;
        }
        deleteEmployerCustomers(employer);
        EntityTransaction transaction = null;
        try (EntityManager entityManager = entityManagerFactory.createEntityManager()) {
            transaction = entityManager.getTransaction();
            transaction.begin();
            entityManager.remove(entityManager.find(Employer.class, employer.getId()));
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
                return false;
            }
        }
        return true;

    }

    @Override
    public void deleteAll(List<Employer> employers) {
        EntityTransaction transaction = null;
        try (EntityManager entityManager = entityManagerFactory.createEntityManager()) {
            transaction = entityManager.getTransaction();
            transaction.begin();
            for (Employer employer : employers) {
                entityManager.remove(entityManager.find(Employer.class, employer.getId()));
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }

    }

    @Override
    public void saveAll(List<Employer> employers) {
        EntityTransaction transaction = null;
        try (EntityManager entityManager = entityManagerFactory.createEntityManager()) {
            transaction = entityManager.getTransaction();
            transaction.begin();
            for (Employer employer : employers) {
                entityManager.persist(employer);
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }

    }

    @Override
    public List<Employer> findAll() {
        try (EntityManager entityManager = entityManagerFactory.createEntityManager()) {
            return entityManager.createQuery("from Employer e", Employer.class).getResultList();
        }

    }

    @Override
    public Employer getOne(long id) {
        try (EntityManager entityManager = entityManagerFactory.createEntityManager()) {
            return entityManager.find(Employer.class, id);
        }

    }

    @Override
    public boolean update(Employer employer){
        EntityTransaction transaction = null;
        try (EntityManager entityManager = entityManagerFactory.createEntityManager()) {
            transaction = entityManager.getTransaction();
            transaction.begin();
            entityManager.merge(employer);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
                return false;
            }
        }
        return true;
    }
    @Override
    public void deleteEmployerCustomers(Employer employer) {
        EntityTransaction transaction = null;
        try (EntityManager entityManager = entityManagerFactory.createEntityManager()) {
            transaction = entityManager.getTransaction();
            transaction.begin();
            entityManager.createNativeQuery("DELETE FROM public.customer_employer WHERE employer_id = :id")
                    .setParameter("id",employer.getId())
                    .executeUpdate();
            transaction.commit();
        } catch (Exception e) {
            if (transaction.isActive()) {
                transaction.rollback();
            }
        }
    }


}
