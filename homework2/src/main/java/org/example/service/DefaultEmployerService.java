package org.example.service;

import org.example.dao.DefaultEmployerDao;
import org.example.dao.EmployerDao;
import org.example.domain.Employer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultEmployerService implements EmployerService {
    private final DefaultEmployerDao employerDao;
    public DefaultEmployerService(DefaultEmployerDao employerDao){
        this.employerDao = employerDao;
    }
    @Override
    public Employer save(Employer employer) {
        return employerDao.save(employer);
    }


    @Override
    public boolean delete(Long id) {
        return employerDao.delete(employerDao.getOne(id));
    }

    @Override
    public void deleteAll(List<Employer> employers) {
        employerDao.deleteAll(employers);
    }

    @Override
    public void saveAll(List<Employer> employers) {
        employerDao.saveAll(employers);
    }

    @Override
    public List<Employer> findAll() {
        return employerDao.findAll();
    }

    @Override
    public Employer getOne(long id) {
        return employerDao.getOne(id);
    }

    @Override
    public boolean update(Employer employer){
        return employerDao.update(employer);
    }
}
