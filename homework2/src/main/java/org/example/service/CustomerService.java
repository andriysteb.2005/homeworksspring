package org.example.service;

import org.example.domain.Account;
import org.example.domain.Currency;
import org.example.domain.Customer;

import java.util.List;

public interface CustomerService {
    Customer save(Customer customer);
    boolean delete(Customer customer);
    void deleteAll(List<Customer> customers);
    void saveAll(List<Customer> customers);
    List<Customer> findAll();
    Customer getOne(long id);
    public boolean update(Customer customer);
    public boolean createAccount(long id, Currency currency);
    public boolean deleteAccount(long id, String number);

}
