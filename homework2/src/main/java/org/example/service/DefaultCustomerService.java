package org.example.service;

import org.example.dao.AccountDao;
import org.example.dao.DefaultCustomerDao;
import org.example.domain.Account;
import org.example.domain.Currency;
import org.example.domain.Customer;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DefaultCustomerService implements CustomerService {
    private final DefaultCustomerDao customerDao;
    private AccountDao accountDao;

    public DefaultCustomerService(DefaultCustomerDao customerDao,  AccountDao accountDao) {
        this.customerDao = customerDao;
        this.accountDao = accountDao;
    }

    @Override
    public Customer save(Customer customer) {
        return customerDao.save(customer);
    }

    @Override
    public boolean delete(Customer customer) {
        return customerDao.delete(customer);
    }

    @Override
    public void deleteAll(List<Customer> customers) {
        customerDao.deleteAll(customers);
    }

    @Override
    public void saveAll(List<Customer> customers) {
        customerDao.saveAll(customers);
    }

    @Override
    public List<Customer> findAll() {
        return customerDao.findAll();
    }


    @Override
    public Customer getOne(long id) {
        return customerDao.getOne(id);
    }

    @Override
    public boolean update(Customer customer) {
        return customerDao.update(customer);
    }
    @Override
    public boolean createAccount(long id, Currency currency){
        Customer customer = getOne(id);
        if (customer == null){
            return false;
        }
        Account account = new Account(currency,customer);
        account.setBalance(0.0);
        accountDao.save(account);
        return true;

    }

    @Override
    public boolean deleteAccount(long id, String number){
        Optional<Account> deleteAccount = customerDao.getOne(id).getAccounts()
                .stream()
                .filter(e -> e.getNumber().equals(number)).
                findFirst();
        if (!deleteAccount.isEmpty()){
            return accountDao.delete(deleteAccount.get());
        }
        return false;

    }



}
