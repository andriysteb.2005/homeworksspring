package org.example.controller;


import org.example.domain.Currency;
import org.example.domain.Customer;
import org.example.service.DefaultCustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/customers")
public class CustomerController {
    private final DefaultCustomerService customerService;
    public CustomerController(DefaultCustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCustomer(@PathVariable Long id) {
        return ResponseEntity.ok(customerService.getOne(id));
    }

    @GetMapping
    public ResponseEntity<?> getAllCustomers() {
        return ResponseEntity.ok(customerService.findAll());
    }

    @PostMapping
    public void create( String name,String email,  Integer age){
        Customer customer = customerService.save(new Customer( name,  email,  age));
        if(customer != null){
            ResponseEntity.ok().body("Created successfully");
        }
        ResponseEntity.notFound().build();
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> updateCustomer(@RequestBody Customer customer) {
        boolean result = customerService.update(customer);
        if(result){
            return ResponseEntity.ok().body("Changed successfully");
        }
        return ResponseEntity.ok().body("Nothing changed");

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable Long id) {
        boolean result = customerService.delete(customerService.getOne(id));
        return result? ResponseEntity.ok().body("Deleted successfully"):ResponseEntity.notFound().build();

    }

    @PostMapping("/{id}/accounts/create")
    public ResponseEntity<?> createAccount(@PathVariable("id") Long id, Currency currency){
        boolean result = customerService.createAccount(id,currency);
        return result? ResponseEntity.ok().body("Created successfully"):ResponseEntity.notFound().build();
    }
    @DeleteMapping("/{id}/accounts/delete")
    public ResponseEntity<?> deleteAccount(@PathVariable("id") Long id, String number){
        boolean result = customerService.deleteAccount(id,number);
        return result? ResponseEntity.ok().body("Deleted successfully"):ResponseEntity.notFound().build();
    }

}

