package org.example.controller;

import org.example.domain.Employer;
import org.example.service.DefaultEmployerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/employers")
public class EmployerController {
    private final DefaultEmployerService employerService;
    public EmployerController(DefaultEmployerService employerService){
        this.employerService = employerService;
    }
        @GetMapping("/{id}")
        public ResponseEntity<?> getEmployer(@PathVariable Long id) {
            return ResponseEntity.ok(employerService.getOne(id));
        }

        @GetMapping
        public ResponseEntity<?> getAllEmployers() {
            return ResponseEntity.ok(employerService.findAll());
        }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id){
        boolean result = employerService.delete(id);
        return result? ResponseEntity.ok().body("Deleted successfully"):ResponseEntity.notFound().build();
    }

    @PutMapping("/update")
    public ResponseEntity<?> update(Employer employer){
        boolean result = employerService.update(employer);
        if(result){
            return ResponseEntity.ok().body("Changed successfully");
        }
        return ResponseEntity.ok().body("Nothing changed");
    }


}
