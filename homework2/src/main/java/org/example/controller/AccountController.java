package org.example.controller;


import org.example.domain.Account;
import org.example.service.DefaultAccountService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AccountController {
    private final DefaultAccountService accountService;

    public AccountController(DefaultAccountService accountService) {
        this.accountService = accountService;
    }
    @GetMapping("/accounts")
    public List<Account> findAll(){
        return accountService.findAll();
    }

    @PutMapping("/deposite")
    public ResponseEntity<?> deposite( String number,double sum){
        boolean result = accountService.depositeAccount(number,sum);
        if(result){
            return ResponseEntity.ok().body("the balance is replenished");
        }
        return ResponseEntity.ok().body("Nothing changed");
    }
    @PutMapping("/withdraw")
    public ResponseEntity<?> withdraw( String number,  double sum){
        boolean result = accountService.withdrawMoney(number,sum);
        if(result){
            return ResponseEntity.ok().body("money were withdrawn successfully");
        }
        return ResponseEntity.ok().body("Nothing changed");
    }



    @PutMapping("/transfer")
    public ResponseEntity<?> transfer( String numberFrom, String numberTo,double sum){
        boolean result = accountService.transferMoney(numberFrom, numberTo, sum);
        if(result){
            return ResponseEntity.ok().body("money were transferred successfully");
        }
        return ResponseEntity.ok().body("Nothing changed");
    }



}
